<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png') ?>">
<title>Administrator Kemitraan</title>
<!-- Bootstrap -->
<link href="<?php echo base_url('asset_admin/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
<!-- Font Awesome -->
<link href="<?php echo base_url('asset_admin/vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
<!-- NProgress -->
<link href="<?php echo base_url('asset_admin/vendors/nprogress/nprogress.css'); ?>" rel="stylesheet">
<!-- bootstrap-daterangepicker -->
<link href="<?php echo base_url('asset_admin/vendors/bootstrap-daterangepicker/daterangepicker.css'); ?>" rel="stylesheet">

<!-- bootstrap-wysiwyg -->
<link href="<?php echo base_url('asset_admin/vendors/google-code-prettify/bin/prettify.min.css'); ?>" rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo base_url('asset_admin/vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">
<!-- Switchery -->
<link href="<?php echo base_url('asset_admin/vendors/switchery/dist/switchery.min.css'); ?>" rel="stylesheet">
<!-- starrr -->
<link href="<?php echo base_url('asset_admin/vendors/starrr/dist/starrr.css'); ?>" rel="stylesheet">


<!-- iCheck -->
<link href="<?php echo base_url('asset_admin/vendors/iCheck/skins/flat/green.css') ?>" rel="stylesheet">
<!-- Datatables -->
<link href="<?php echo base_url('asset_admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('asset_admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('asset_admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('asset_admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('asset_admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') ?>" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url('asset_admin/vendors/summernote/dist/summernote.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('asset_admin/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'); ?>">


<!-- Custom Theme Style -->
<link href="<?php echo base_url('asset_admin/build/css/custom.min.css'); ?>" rel="stylesheet">