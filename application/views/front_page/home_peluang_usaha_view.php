<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>Peluang Usaha Malond (Manuk Londo) Jawa Timur | Peluang Usaha Kuliner Manuk Londo - Malon Mojokerto | Jual Beli Malond Mojokerto</title>

	<meta name="description" content="Manuk Londo Mojokerto, Kuliner Terbaru Mojokerto Malond (Manuk Londo) Adalah Burung Hasil Persilangan Burung Prancis (french Quail) Yang Telah Dikembangbiakkan Di Yogyakarta Sejak Tahun 2010. Kami Selaku Distributor Tunggal Malond Di Sidoarjo & Mojokerto Siap Menyuplai Kebutuhan Karkas Malond Untuk Usaha Kuliner Anda.">
	<meta name="author" content="Malond Mojokerto">
	<meta name="keywords" content="kuliner mojokerto baru, kuliner mojokerto <?php echo date('Y'); ?>, malond mojokerto, jajanan mojokerto, mojokerto, restoran mojokerto, hotel mojokerto, bisnis di mojokerto, harga malond <?php echo date('Y')?>" />
        
        <link rel="canonical" href="http://www.ikiboom.com/" />
	<meta name="robots" content="follow,index" />  

	<meta property="og:url"           content="http://www.ikiboom.com/" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="Agen Malond - Malon Mojokerto | Kuliner Terbaru Mojokerto | Kuliner Manuk Londo - Malon Mojokerto | Jual Beli Malond Mojokerto" />
	<meta property="og:description"   content="Kuliner Terbaru Mojokerto Malond (Manuk Londo) Adalah Burung Hasil Persilangan Burung Prancis (french Quail) Yang Telah Dikembangbiakkan Di Yogyakarta Sejak Tahun 2010. Kami Selaku Distributor Tunggal Malond Di Sidoarjo & Mojokerto Siap Menyuplai Kebutuhan Karkas Malond Untuk Usaha Kuliner Anda." />
	<meta property="og:image"         content="<?php echo base_url('asset_front/image/galeri/img2.png'); ?>" /> 

	<link rel="icon" href="<?php echo base_url('asset_front/image/logo_1.png'); ?>">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('asset_peluang_usaha/css/bootstrap.min.css'); ?> " />

	<!-- Owl Carousel -->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('asset_peluang_usaha/css/owl.carousel.css'); ?> " />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('asset_peluang_usaha/css/owl.theme.default.css'); ?> " />

	<!-- Magnific Popup -->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('asset_peluang_usaha/css/magnific-popup.css'); ?> " />

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="<?php echo base_url('asset_peluang_usaha/css/font-awesome.min.css'); ?> ">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('asset_peluang_usaha/css/style.css'); ?> " />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<!-- Header -->
	<header id="home">
		<!-- Background Image -->
		<div class="bg-img" style="background-image: url('asset_peluang_usaha/image/backround1.png');">
			<div class="overlay"></div>
		</div>
		<!-- /Background Image -->

		<!-- home wrapper -->
		<div class="home-wrapper">
			<div class="container">
				<div class="row">

					<!-- home content -->
					<div class="col-md-10 col-md-offset-1">
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="img-brand">
									<img src="<?php echo base_url('asset_peluang_usaha/image/kemasan_iki_malond.png'); ?>" class="img-responsive" alt="Image">
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="deskripsi_brand">
									<h4>wegosales.com & IKI GROUP Jawa Timur, present:</h4>
									<h5>
										PELUANG USAHA
									</h5>
									<h3>Di Jawa Timur</h3>
								</div>
							</div>
						</div>

						<div class="deskripsi_header">
							<p>
								Jangan Lewatkan Kesempatan Emas ini!! Surabaya, Malang, Mojokerto, Sidoarjo & Seluruh Jawa Timur, <br>
								<span class="sm-setfont">Gratis!! TANPA BIAYA!! TANPA MINIMAL ORDER!!</span>
							</p>
						</div>
					</div>
					<!-- /home content -->

				</div>
			</div>
		</div>
		<!-- /home wrapper -->

	</header>
	<!-- /Header -->

	<div id="step-by-step" class="section sm-padding">
		<div class="container-fluid">
			<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
				<div class="img-money">
					<img src="<?php echo base_url('asset_peluang_usaha/image/asset1.png'); ?>" class="img-responsive" alt="Image">
				</div>
			</div>
			<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
				<div class="deskripsi-step">
					<p>
						Dapatkan Penghasilan <span class="text-step-bold">Ratusan Ribu</span> Hingga <span class="text-step-bold">Jutaan Rupiah</span> Tiap Minggunya Dengan Bisnis <span class="text-step-bold">IKI MALOND</span>.
					</p>
					<h2>Manuk Londo Berbumbu, Siap Goreng!!</h2>
					<p><span class="set-font-lg">Ayo Ajak Keluarga, Tetangga, Teman, Kerabapt Anda Untuk menikmati lezatnya berbisnis MALOND</span></p>
				</div>
			</div>

			<div class="step-list">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 pd-set-1">
					<div class="deskripsi-list-step">
						<p>
							KENAPA ANDA HARUS BERJUALAN IKI MALOND ?
						</p>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 pd-set-1 set-top-xs">
					<div class="frame-laps1">
						<img src="<?php echo base_url('asset_peluang_usaha/image/asset3.png'); ?>" class="img-responsive" alt="Image">
						<div class="frame-step">
							<span class="number-step">1</span>
							<h2>MODAL KECIL</h2>
							<p>Harga Sangat TERJANGKAU Hanya 20rbuan/pax isi 2 ekor. Cocok untuk segala kalangan</p>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 pd-set-1 set-top-xs">
					<div class="frame-laps1">
						<img src="<?php echo base_url('asset_peluang_usaha/image/asset3.png'); ?>" class="img-responsive" alt="Image">
						<div class="frame-step">
							<span class="number-step">2</span>
							<h2>TANPA MASAK</h2>
							<p>
								IKI Malond Tersedia 3 Varian Rasa (Bumbu Kuning, Rica-Rica, Balado), tinggal goreng saja.
							</p>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 pd-set-1 set-top-md set-down-xs">
					<div class="frame-laps1">
						<img src="<?php echo base_url('asset_peluang_usaha/image/asset3.png'); ?>" class="img-responsive" alt="Image">
						<div class="frame-step">
							<span class="number-step">3</span>
							<h2>MODAL KECIL</h2>
							<p>Harga Sangat TERJANGKAU Hanya 20rbuan/pax isi 2 ekor. Cocok untuk segala kalangan</p>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 pd-set-1 set-top-md set-down-xs">
					<div class="frame-laps1">
						<img src="<?php echo base_url('asset_peluang_usaha/image/asset3.png'); ?>" class="img-responsive" alt="Image">
						<div class="frame-step">
							<span class="number-step">4</span>
							<h2>TANPA BIAYA</h2>
							<p>
								Tanpa Biaya Franchise/Waralaba Tanpa Syarat Jumlah Minimal Pengambilan Awal
							</p>
						</div>
					</div>
				</div>
				
			</div>
			
		</div>
	</div>

	<div id="keunggulan-produk" class="section md-padding">
		<img src="<?php echo base_url('asset_peluang_usaha/image/backround4.png'); ?>" class="img-responsive" alt="Image">
	</div>

	<div id="produk" class="section">
		<div class="container-fluid">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
				<div class="deskripsi-produk">
					<h1>Sudah Punya</h1>
					<h3>Usaha KULINER Sendiri ? 
						<span class="color-sweetpotato">Ingin Memakai Menu MALOND-Manuk Londo ?</span></h3>
					<h4>
						TENANG!!! Kami Juga Menyediakan Karkas Malond Untuk Kebutuhan Kuliner Anda.
					</h4>
					<h4>
						Malond Dapat Diolah Dalam Berbagai Jenis Olahan Sesuai Selera Konsumen Anda, Goreng, Bacem, Pedas, Oriental, Western, Dsb.
					</h4>
					<h4>
						Malond Cocok Untuk Warung Makan, Pecel Lele, Lesehan, Catring, Cafe, Restoran Hingga Hotel Anda.
					</h4>
					<h2>
						HARGA PEREKOR MULAI RP 8ribuan SAJA! <br> MURAH MERIAH
					</h2>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-7">
				<img src="<?php echo base_url('asset_peluang_usaha/image/img1.png'); ?>" class="img-responsive" alt="Image">
			</div>
		</div>

		<div class="container-fluid">
			<div class="list-produk">
				
				<div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 pd-produk">
					<div class="frame-produk">
						<h2 class="text-produk">
							PRODUK MALOND
						</h2>
						<p>
							Kami Sediakan <br><span class="text-produk-bold">5 Pilihan Grade Malond</span>  Sesuai Kebutuhan Anda
						</p>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 pd-produk">
					<div class="produk-list">
						<div class="frame-list-produk">
						<h4 class="name-produk">
						MALOND GRADE RESTO
						</h4>
						<img src="<?php echo base_url('asset_peluang_usaha/image/produk/malond_resto.png'); ?>" class="img-responsive img-produk" alt="Image">
						</div>
						<table class="detail-produk">
							<tbody>
								<tr>
									<td>Berat &nbsp; :</td>
									<td>&nbsp;225 gr</td>
								</tr>
								<tr>
									<td>Isi/Pack :</td>
									<td>&nbsp;5 Ekor</td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 pd-produk">
					<div class="produk-list">
						<div class="frame-list-produk">
						<h4 class="name-produk">
						MALOND GRADE A
						</h4>
						<img src="<?php echo base_url('asset_peluang_usaha/image/produk/malond_a.png'); ?>" class="img-responsive img-produk" alt="Image">
						</div>
						<table class="detail-produk">
							<tbody>
								<tr>
									<td>Berat &nbsp; :</td>
									<td>&nbsp;205 - 224 gr</td>
								</tr>
								<tr>
									<td>Isi/Pack :</td>
									<td>&nbsp;5 Ekor</td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 pd-produk">
					<div class="produk-list">
						<div class="frame-list-produk">
						<h4 class="name-produk">
						MALOND GRADE B
						</h4>
						<img src="<?php echo base_url('asset_peluang_usaha/image/produk/malond_b.png'); ?>" class="img-responsive img-produk" alt="Image">
						</div>
						<table class="detail-produk">
							<tbody>
								<tr>
									<td>Berat &nbsp; :</td>
									<td>&nbsp;185 - 204 gr</td>
								</tr>
								<tr>
									<td>Isi/Pack :</td>
									<td>&nbsp;5 Ekor</td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 pd-produk">
					<div class="produk-list">
						<div class="frame-list-produk">
						<h4 class="name-produk">
						MALOND GRADE C
						</h4>
						<img src="<?php echo base_url('asset_peluang_usaha/image/produk/malond_c.png'); ?>" class="img-responsive img-produk" alt="Image">
						</div>
						<table class="detail-produk">
							<tbody>
								<tr>
									<td>Berat &nbsp; :</td>
									<td>&nbsp;165 - 184 gr</td>
								</tr>
								<tr>
									<td>Isi/Pack :</td>
									<td>&nbsp;5 Ekor</td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 pd-produk">
					<div class="produk-list">
						<div class="frame-list-produk">
						<h4 class="name-produk">
						MALOND GRADE D
						</h4>
						<img src="<?php echo base_url('asset_peluang_usaha/image/produk/malond_d.png'); ?>" class="img-responsive img-produk" alt="Image">
						</div>
						<table class="detail-produk">
							<tbody>
								<tr>
									<td>Berat &nbsp; :</td>
									<td>&nbsp;142 - 164 gr</td>
								</tr>
								<tr>
									<td>Isi/Pack :</td>
									<td>&nbsp;10 Ekor</td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>

			</div>

		</div>
	</div>

	<div id="box-malond" class="section ">
		<img src="<?php echo base_url('asset_peluang_usaha/image/backround5.png'); ?>" class="img-responsive" alt="Image">
	</div>

	<div id="keuntungan" class="section ">
		<img src="<?php echo base_url('asset_peluang_usaha/image/keuntungan.png'); ?>" class="img-responsive" alt="Image">
	</div>

	<div id="kontak-sales" class="section bg-white-flat">
		<div class="container-fluid">
			<h3 class="title-kontak-sales">Dimana Anda Mendapat <br> Produk Malond dari IKI GROUP ?</h3>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="kontak-name-sales">
					<h3>MOJOKERTO, SIDOARJO <br> & SEKITARNYA</h3>
					<p>
						<span class="text-kontak-bold">CHAIRUL RACHMAD HIDAYAT</span> <br>
						0813 3490 2657 <br>
						c.rhidayat13@gmail.com <br>
						<a href="http://www.ikiboom.com/" target="_blank">www.ikiboom.com</a>
					</p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="kontak-name-sales">
					<h3>SURABAYA, MALANG <br> & SEKITARNYA</h3>
					<p>
						<span class="text-kontak-bold">IDDO</span> <br>
						08113691914 & 085102781989 (WA) <br>
						iddozechariah25@gmail.com <br>
						<a href="http://www.ikicarnis.com/" target="_blank">www.ikicarnis.com</a>
					</p>
				</div>
			</div>
		</div>
		
	</div>


	<!-- Footer -->
	<footer id="footer" class="sm-padding bg-purple">

		<!-- Container -->
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<h2 class="footer-text">
						IKI GROUP <br> JAWA TIMUR
					</h2>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="footer-tentang">
						<h3>Tentang Kami</h3>
						<p>
							Kami selaku distributor tunggal Malond <span class="text-footer-bold">di JAWA TIMUR Siap Menyuplai Kebutuhan Karkas Malond Untuk Usaha Kuliner dan Oleh-oleh Anda</span>
							Kami juga memberi kesempatan kepada Anda untuk menjadi Agen kami di wilayah surabaya dan sekitarnya. 
						</p>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
					<div class="footer-kontak">
						<h3>CHAIRUL RACHMAD HIDAYAT </h3>
						<p>0813 3490 2657 <br>c.rhidayat13@gmail.com </p>
					</div>
					<div class="footer-kontak">
						<h3>IDDO</h3>
						<p>0811 369 1914 / 0851 0278 1989 (WA) <br>iddozechariah25@gmail.com </p>
					</div>
				</div>
				<div class="col-xs-6 col-sm-12 col-md-3 col-lg-3">
					<div class="logo-mui">
						<img src="<?php echo base_url('asset_peluang_usaha/image/halal_icon.png'); ?>" class="img-responsive" alt="Image">
					</div>
					<h3 class="name-mui">COPYRIGHT <br> 2018</h3>
				</div>
			</div>
		</div>
		<!-- /Container -->

	</footer>
	<!-- /Footer -->



	<!-- Back to top -->
	<div id="back-to-top"></div>
	<!-- /Back to top -->

	<!-- Preloader -->
	<!-- <div id="preloader">
		<div class="preloader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div> -->
	<!-- /Preloader -->

	<!-- jQuery Plugins -->
	<script type="text/javascript" src="<?php echo base_url('asset_peluang_usaha/js/jquery.min.js'); ?> "></script>
	<script type="text/javascript" src="<?php echo base_url('asset_peluang_usaha/js/bootstrap.min.js'); ?> "></script>
	<script type="text/javascript" src="<?php echo base_url('asset_peluang_usaha/js/owl.carousel.min.js'); ?> "></script>
	<script type="text/javascript" src="<?php echo base_url('asset_peluang_usaha/js/jquery.magnific-popup.js'); ?> "></script>
	<script type="text/javascript" src="<?php echo base_url('asset_peluang_usaha/js/main.js'); ?> "></script>

</body>

</html>
