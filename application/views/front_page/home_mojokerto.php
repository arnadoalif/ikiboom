﻿<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>Agen Malond - Malon Mojokerto | Kuliner Terbaru Mojokerto | Kuliner Manuk Londo - Malon Mojokerto | Jual Beli Malond Mojokerto</title>

	<meta name="description" content="Kuliner Terbaru Mojokerto Malond (Manuk Londo) Adalah Burung Hasil Persilangan Burung Prancis (french Quail) Yang Telah Dikembangbiakkan Di Yogyakarta Sejak Tahun 2010. Kami Selaku Distributor Tunggal Malond Di Sidoarjo & Mojokerto Siap Menyuplai Kebutuhan Karkas Malond Untuk Usaha Kuliner Anda.">
	<meta name="author" content="Malond Mojokerto">
	<meta name="keywords" content="kuliner mojokerto baru, kuliner mojokerto <?php echo date('Y'); ?>, malond mojokerto, jajanan mojokerto, mojokerto, restoran mojokerto, hotel mojokerto, bisnis di mojokerto, harga malond <?php echo date('Y')?>" />
        
    <link rel="canonical" href="http://www.ikiboom.com/" />
	<meta name="robots" content="follow,index" /> 

	<meta property="og:url"           content="http://www.ikiboom.com/" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="Agen Malond - Malon Mojokerto | Kuliner Terbaru Mojokerto | Kuliner Manuk Londo - Malon Mojokerto | Jual Beli Malond Mojokerto" />
	<meta property="og:description"   content="Kuliner Terbaru Mojokerto Malond (Manuk Londo) Adalah Burung Hasil Persilangan Burung Prancis (french Quail) Yang Telah Dikembangbiakkan Di Yogyakarta Sejak Tahun 2010. Kami Selaku Distributor Tunggal Malond Di Sidoarjo & Mojokerto Siap Menyuplai Kebutuhan Karkas Malond Untuk Usaha Kuliner Anda." />
	<meta property="og:image"         content="<?php echo base_url('asset_front/image/galeri/img2.png'); ?>" />       

    <meta name="google-site-verification" content="D_28_PfaAwoGWHgpZpcjjWYFc3-tv1ldAI5xz297ouk" />
    <link rel="icon" href="<?php echo base_url('asset_front/image/logo_1.png'); ?>">
	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('asset_front/css/bootstrap.min.css'); ?> " />

	<!-- Owl Carousel -->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('asset_front/css/owl.carousel.css'); ?> " />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('asset_front/css/owl.theme.default.css'); ?> " />

	<!-- Magnific Popup -->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('asset_front/css/magnific-popup.css'); ?> " />

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="<?php echo base_url('asset_front/css/font-awesome.min.css'); ?> ">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('asset_front/css/style.css'); ?> " />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<!-- Header -->
	<header id="home">
		<!-- Background Image -->
		<div class="bg-img" style="background-image: url('<?php echo base_url("asset_front/image/background1.png"); ?>');">
			<div class="overlay"></div>
		</div>
		<!-- /Background Image -->

		<!-- home wrapper -->
		<div class="home-wrapper">
			<div class="container">
				<div class="row">

					<!-- home content -->
					<div class="col-md-10 col-md-offset-1">
						<div class="home-content">
							<img src="<?php echo base_url('./asset_front/image/logo_1.png'); ?>" class="logo_head" alt="">
							
							<p class="head-text">
								Mau Bisnis Kuliner Hits di Sidoarjo & Mojokerto ? Mari <span class="text-yellow">Berbisnis</span> Kuliner <span class="text-yellow">MALOND </span> Dapatkan <span class="text-yellow">Penghasilan Jutaan</span> Rupiah Setiap Minggunya!
							</p>
						</div>
					</div>
					<!-- /home content -->

				</div>
			</div>
		</div>
		<!-- /home wrapper -->
	</header>
	<!-- /Header -->

	<!-- Service -->
	<div id="head-two" class="section md-padding ">
		<div class="bg-img" style="background-image: url('./asset_front/image/background2.png');">
		</div>
		<div class="headtwo-title">
			<p>
				Kuliner Untuk Semua Kalangan
			</p>
		</div>
		<div class="name-backround">
			<img src="<?php echo base_url('./asset_front/image/logo_mui.png'); ?>" alt="">
			<div class="desription-headtwo">
				<p class="name-headtwo">
					Kini Telah Hadir Di Sidoarjo & Mojokerto, Bisnis Kuliner Hits “MALOND”.
					<br>
					Sangat Cocok Untuk Usaha Kuliner Kantin Perusahaan, Pecel, Sate, Rumah Makan, Hingga Café & Restoran Anda Dengan Harga Yang Sangat Terjangkau.
					<br>
					Rasa Dan Tekstur Daging Malond Yang Khas Membuat  Konsumen Anda Ketagihan Untuk Menyantap Menu Malond.
				</p>
			</div>
		</div>
	</div>
	<!-- /Service -->

	<!-- Why Choose Us -->
	<div id="features" class="section bg-primary ">

		<!-- Container -->
		<div class="container-fluid">

			<div class="features-label">
				
				<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
						<div class="img-features">
							<img src="<?php echo base_url('asset_front/image/icon_1.png'); ?>" class="img-responsive" alt="Image">
						</div>
						<div class="name-features">
							<p>
								Harga <br> Terjangkau
							</p>
						</div>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 lineleft-features">
						<div class="img-features">
							<img src="<?php echo base_url('asset_front/image/icon_2.png'); ?>" class="img-responsive" alt="Image">
						</div>
						<div class="name-features">
							<p>
								Mudah <br> Diolah
							</p>
						</div>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 line-features lineleft-features">
						<div class="img-features">
							<img src="<?php echo base_url('asset_front/image/icon_3.png'); ?>" class="img-responsive" alt="Image">
						</div>
						<div class="name-features">
							<p>
								Gizi <br> Tinggi
							</p>
						</div>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 m-top">
						<div class="img-features">
							<img src="<?php echo base_url('asset_front/image/icon_4.png'); ?>" class="img-responsive max-40" alt="Image">
						</div>
						<div class="name-features">
							<p>
								Sertifikasi <br> Halal
							</p>
						</div>
					</div>

			</div>

		</div>
		<!-- /Container -->

	</div>
	<!-- /Why Choose Us -->

	<!-- About -->
	<div id="about" class="section md-padding bg-white">

		<div class="slide-malond">
			<div class="container">
				
			
				<div class="product-slider">
				  <div id="carousel" class="carousel slide" data-ride="carousel">
				    <div class="carousel-inner">
				      <div class="item active"><img src="<?php echo base_url('asset_front/image/slide1_1300x824.png'); ?>"> </div>
				      <div class="item"> <img src="<?php echo base_url('asset_front/image/slide2_1300x824.png'); ?>"> </div>
				      <div class="item"> <img src="<?php echo base_url('asset_front/image/slide3_1300x824.png'); ?>"> </div>
				      <div class="item"> <img src="<?php echo base_url('asset_front/image/slide1_1300x824.png'); ?>"> </div>
				    </div>
				  </div>
				  <div class="clearfix">
				    <div id="thumbcarousel" class="carousel slide" data-interval="false">
				      <div class="carousel-inner">
				        <div class="item active">
				          <div data-target="#carousel" data-slide-to="0" class="thumb"><img src="<?php echo base_url('asset_front/image/slide1_300x190.png'); ?>"></div>
				          <div data-target="#carousel" data-slide-to="1" class="thumb"><img src="<?php echo base_url('asset_front/image/slide2_300x190.png'); ?>"></div>
				          <div data-target="#carousel" data-slide-to="2" class="thumb"><img src="<?php echo base_url('asset_front/image/slide3_300x190.png'); ?>"></div>
				          <div data-target="#carousel" data-slide-to="3" class="thumb"><img src="<?php echo base_url('asset_front/image/slide1_300x190.png'); ?>"></div>
				        </div>
				      </div>
				      <!-- /carousel-inner --> 
				      <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a> <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next"><i class="fa fa-angle-right" aria-hidden="true"></i> </a> </div>
				    <!-- /thumbcarousel --> 
				    
				  </div>
				</div>

			</div>

		</div>		

		<div class="about-product">
			<div class="produk-text">
				<h2>APA ITU <span class="text-wight text-wbold">MALOND</span> ?</h2>
				<p>
					Malond (manuk Londo) Adalah Burung Hasil Persilangan Burung Prancis (french Quail) Yang Telah Dikembangbiakkan Di Yogyakarta Sejak Tahun 2010.
				</p>
				<p>
					Dengan Usia Potong Malond 35 Hari Membuat Tekstur, Taste (rasa), Aroma Khas Dan Keempukan Dagingnya Optimal Sehingga Bumbu Mudah Meresap Di Dalamnya.
                    Malond Dapat Diolah  Dalam Berbagai Macam Masakan, Traditional, Western, Oriental, Dan Lainnya Sesuai Dengan Selera Konsumen Anda.
				</p>
				<p>
					<span class="text-wight text-wbold">Kami Selaku Distributor Tunggal Malond Di Sidoarjo & Mojokerto Siap Menyuplai Kebutuhan Karkas Malond Untuk Usaha Kuliner Anda.</span> Segera Tingkatkan Omset Penjualan Anda Dengan Menu Malond, Menu Fenomenal Yang Telah Sukses Di Yogyakarta, Jateng Dan Jabodetabek.
				</p>
			</div>
		</div>

	</div>
	<!-- /About -->

	<!-- Service -->
	<div id="head-three" class="section ">
		<div class="three-bg">
			<img class="img-desktop" src="<?php echo base_url('asset_front/image/background6.png'); ?>" alt="">
			<!-- <img class="hidden-lg hidden-md" src="<?php echo base_url('asset_front/image/m_background5.png'); ?>" alt=""> -->
		</div>			
	</div>
	<!-- /Service -->

	<div class="section pic-galeri">
		<div class="container">
			<div class="product-slider">
			  <div id="carousel" class="carousel slide" data-ride="carousel">
			    <div class="carousel-inner">
			      <div class="item active"><img src="<?php echo base_url('asset_front/image/slide_big_1.png'); ?>"> </div>
			      <div class="item"> <img src="<?php echo base_url('asset_front/image/slide_big_2.png'); ?>"> </div>
			      <div class="item"> <img src="<?php echo base_url('asset_front/image/slide_big_3.png'); ?>"> </div>
			      <div class="item"> <img src="<?php echo base_url('asset_front/image/slide_big_4.png'); ?>"> </div>
			    </div>
			  </div>
			  <div class="clearfix">
			    <div id="thumbcarousel" class="carousel slide" data-interval="false">
			      <div class="carousel-inner">
			        <div class="item active">
			          <div data-target="#carousel" data-slide-to="0" class="thumb"><img src="<?php echo base_url('asset_front/image/slide_sm_1.png'); ?>"></div>
			          <div data-target="#carousel" data-slide-to="1" class="thumb"><img src="<?php echo base_url('asset_front/image/slide_sm_2.png'); ?>"></div>
			          <div data-target="#carousel" data-slide-to="2" class="thumb"><img src="<?php echo base_url('asset_front/image/slide_sm_3.png'); ?>"></div>
			          <div data-target="#carousel" data-slide-to="3" class="thumb"><img src="<?php echo base_url('asset_front/image/slide_sm_4.png'); ?>"></div>
			        </div>
			      </div>
			      
			      <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a> <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next"><i class="fa fa-angle-right" aria-hidden="true"></i> </a> </div>
			  </div>
			</div>
		</div>
	</div>

	<div id="produk" class="section bg-white produk">

		<div class="container-fluid">

			<div class="menu-menu">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="produk-menudesc">
						<p>
							Kini Saatnya Anda 
							Menjadi <span class="text-drak">Pengusaha 
							Kuliner SUKSES</span> 
							Berkat Olahan <span class="text-drak">MALOND</span> 
							Buktikan Sendiri Hasilnya!
						</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="produk-menu">
						<img src="<?php echo base_url('asset_front/image/pic_menu.png'); ?>" class="img-responsive" alt="Image">
					</div>
				</div>
			</div>

			<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<div class="panel panel-info">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-bell-o"></i> Malond Hidup</h3>
							</div>
							<div class="panel-body">
								<img src="<?php echo base_url('asset_front/image/icon_kandang.png'); ?>" class="img-responsive img-kandang" alt="Image">
								<p class="number-stok" align="center"><?php echo number_format($data_plasma->Jumlah,0,",","."); ?></p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-truck"></i> Stok Malond</h3>
							</div>
							<div class="panel-body">
								<div class="btn-group-vertical">
									<a class="btn btn-default" role="button" data-toggle="modal" href='#modal-id' title="Click Untuk Lihat Produk">
										<img src="<?php echo base_url('./asset_front/image/produk/iki_malond_new.png'); ?>" class="img-responsive img-stok" alt="Image"> 
										<span class="new-nameproduk">Iki Malond <sup>NEW</sup></span> 
										<span class="badge badge-new">Tersedia</span> 
									</a>
									<a class="btn btn-default" role="button">
										<img src="<?php echo base_url('./asset_front/image/produk/malond_resto.png'); ?>" class="img-responsive img-malond" alt="Image"> 
										<span class="nameproduk">Grade Resto</span> 
										<span class="badge badge-stok"><?php echo $data_stok[3]->Jumlah ?></span>
									</a>
									<a class="btn btn-default" role="button">
										<img src="<?php echo base_url('./asset_front/image/produk/malond_a.png'); ?>" class="img-responsive img-malond left-img" alt="Image"> 
										<span class="nameproduk left-name">Grade A</span> 
										<span class="badge badge-stok right-stok"><?php echo $data_stok[0]->Jumlah ?></span> 
									</a>
									<a class="btn btn-default" role="button">
										<img src="<?php echo base_url('./asset_front/image/produk/malond_b.png'); ?>" class="img-responsive img-malond left-img" alt="Image"> 
										<span class="nameproduk left-name">Grade B</span> 
										<span class="badge badge-stok right-stok"><?php echo $data_stok[1]->Jumlah ?></span> 
									</a>
									<a class="btn btn-default" role="button">
										<img src="<?php echo base_url('./asset_front/image/produk/malond_c.png'); ?>" class="img-responsive img-malond left-img" alt="Image"> 
										<span class="nameproduk left-name">Grade C</span> 
										<span class="badge badge-stok right-stok"><?php echo $data_stok[2]->Jumlah ?></span> 
									</a>
									<a class="btn btn-default" role="button">
										<img src="<?php echo base_url('./asset_front/image/produk/malond_d.png'); ?>" class="img-responsive img-malond left-img" alt="Image"> 
										<span class="nameproduk left-name">Grade D</span> 
										<span class="badge badge-stok right-stok"><?php echo $data_stok[4]->Jumlah ?></span> 
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
						<div class="panel panel-danger">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-shopping-cart"></i> Malond Terjual</h3>
							</div>
							<div class="panel-body">
								<div class="btn-group-vertical">
									<a class="btn btn-default" role="button" data-toggle="modal" href='#modal-id' title="Click Untuk Lihat Produk">
										<img src="<?php echo base_url('./asset_front/image/produk/iki_malond_new.png'); ?>" class="img-responsive img-stok" alt="Image"> 
										<span class="new-nameproduk">Iki Malond <sup>NEW</sup></span> 
										<span class="badge badge-new">Tersedia</span> 
									</a>
									<a class="btn btn-default" role="button">
										<img src="<?php echo base_url('./asset_front/image/produk/malond_resto.png'); ?>" class="img-responsive img-malond" alt="Image"> 
										<span class="nameproduk">Grade Resto</span> 
										<span class="badge badge-stok"><?php echo $data_malond_terjual[4]->Hasil ?></span>
									</a>
									<a class="btn btn-default" role="button">
										<img src="<?php echo base_url('./asset_front/image/produk/malond_a.png'); ?>" class="img-responsive img-malond left-img" alt="Image"> 
										<span class="nameproduk left-name">Grade A</span> 
										<span class="badge badge-stok right-stok"><?php echo $data_malond_terjual[0]->Hasil ?></span> 
									</a>
									<a class="btn btn-default" role="button">
										<img src="<?php echo base_url('./asset_front/image/produk/malond_b.png'); ?>" class="img-responsive img-malond left-img" alt="Image"> 
										<span class="nameproduk left-name">Grade B</span> 
										<span class="badge badge-stok right-stok"><?php echo $data_malond_terjual[1]->Hasil ?></span> 
									</a>
									<a class="btn btn-default" role="button">
										<img src="<?php echo base_url('./asset_front/image/produk/malond_c.png'); ?>" class="img-responsive img-malond left-img" alt="Image"> 
										<span class="nameproduk left-name">Grade C</span> 
										<span class="badge badge-stok right-stok"><?php echo $data_malond_terjual[2]->Hasil ?></span> 
									</a>
									<a class="btn btn-default" role="button">
										<img src="<?php echo base_url('./asset_front/image/produk/malond_d.png'); ?>" class="img-responsive img-malond left-img" alt="Image"> 
										<span class="nameproduk left-name">Grade D</span> 
										<span class="badge badge-stok right-stok"><?php echo $data_malond_terjual[5]->Hasil ?></span> 
									</a>
								</div>
							</div>
						</div>
					</div>
			</div>

			<div class="modal fade" id="modal-id">
				<div class="modal-dialog">
					<div class="modal-content">
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						  <div class="carousel-inner">
						    <div class="item active">
						     <img class="img-responsive" src="<?php echo base_url('asset_front/image/produk/iki_malond_produk_1_lg.png'); ?>" alt="">
						    </div>
						    <div class="item">
						      <img class="img-responsive" src="<?php echo base_url('asset_front/image/produk/iki_malond_produk_2_lg.png'); ?>" alt="">
						    </div>
						     <div class="item">
						      <img class="img-responsive" src="<?php echo base_url('asset_front/image/produk/iki_malond_produk_3_lg.png'); ?>" alt="">
						    </div>
						  </div>
						  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						    <i class="fa fa-chevron-circle-left"></i>
						  </a>
						  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						     <i class="fa fa-chevron-circle-right"></i>
						  </a>
						</div>
					</div>
				</div>
			</div>

			<div class="produk-malond">

				<div class="row">

					

					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 pd-produk">
						<div class="produk-frame">
							<div class="text-produk">
								<h2>PRODUK MALOND</h2>
								<p>Kami Sediakan <br><span class="text-bold"> 5 Pilihan Grade Malond </span> Sesuai Kebutuhan Anda</p>
							</div>
						</div>
					</div>

					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 pd-produk">
						<div class="produk-frame">
							<div class="img-produk img-iki">
								<img src="<?php echo base_url('asset_front/image/produk/iki_malond_produk_1.png'); ?>" class="img-responsive">
							</div>
							<div class="produk-name">
								<h4 style="font-family: afternight;">Bumbu Kuning</h4>
								<table class="table-produk">
									<tr>
										<td>Berat &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;350 Gram</td>
									</tr>
									<tr>
										<td>Isi/Pack &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;2 Ekor</td>
									</tr>
									<tr>
										<td>Stok &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp; Tersedia</td>
									</tr>
								</table>
							</div>
						</div>
					</div>

					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 pd-produk">
						<div class="produk-frame">
							<div class="img-produk img-iki">
								<img src="<?php echo base_url('asset_front/image/produk/iki_malond_produk_2.png'); ?>" class="img-responsive">
							</div>
							<div class="produk-name">
								<h4 style="font-family: afternight;">Balado</h4>
								<table class="table-produk">
									<tr>
										<td>Berat &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;350 Gram</td>
									</tr>
									<tr>
										<td>Isi/Pack &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;2 Ekor</td>
									</tr>
									<tr>
										<td>Stok &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;Tersedia</td>
									</tr>
								</table>
							</div>
						</div>
					</div>

					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 pd-produk">
						<div class="produk-frame">
							<div class="img-produk img-iki">
								<img src="<?php echo base_url('asset_front/image/produk/iki_malond_produk_3.png'); ?>" class="img-responsive">
							</div>
							<div class="produk-name">
								<h4 style="font-family: afternight;">Rica - rica</h4>
								<table class="table-produk">
									<tr>
										<td>Berat &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;350 Gram</td>
									</tr>
									<tr>
										<td>Isi/Pack &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;2 Ekor</td>
									</tr>
									<tr>
										<td>Stok &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;Tersedia</td>
									</tr>
								</table>
							</div>
						</div>
					</div>


					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 pd-produk">
						<div class="produk-frame">
							<div class="img-produk">
								<img src="<?php echo base_url('asset_front/image/produk/malond_resto.png'); ?>" class="img-responsive">
							</div>
							<div class="produk-name">
								<h4>MALOND GRADE RESTO</h4>
								<table class="table-produk">
									<tr>
										<td>Berat &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;225 Gram</td>
									</tr>
									<tr>
										<td>Isi/Pack &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;5 Ekor</td>
									</tr>
									<tr>
										<td>Stok &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;<?php echo $data_malond[4]->JumAkhir ?></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 pd-produk">
						<div class="produk-frame">
							<div class="img-produk">
								<img src="<?php echo base_url('asset_front/image/produk/malond_a.png'); ?>" class="img-responsive">
							</div>
							<div class="produk-name">
								<h4>MALOND GRADE A</h4>
								<table class="table-produk">
									<tr>
										<td>Berat &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;205 - 224 Gram</td>
									</tr>
									<tr>
										<td>Isi/Pack &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;5 Ekor</td>
									</tr>
									<tr>
										<td>Stok &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;<?php echo $data_malond[0]->JumAkhir ?></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 pd-produk">
						<div class="produk-frame">
							<div class="img-produk">
								<img src="<?php echo base_url('asset_front/image/produk/malond_b.png'); ?>" class="img-responsive">
							</div>
							<div class="produk-name">
								<h4>MALOND GRADE B</h4>
								<table class="table-produk">
									<tr>
										<td>Berat &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;185 - 204 Gram</td>
									</tr>
									<tr>
										<td>Isi/Pack &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;5 Ekor</td>
									</tr>
									<tr>
										<td>Stok &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;<?php echo $data_malond[1]->JumAkhir ?></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 pd-produk">
						<div class="produk-frame">
							<div class="img-produk">
								<img src="<?php echo base_url('asset_front/image/produk/malond_c.png'); ?>" class="img-responsive">
							</div>
							<div class="produk-name">
								<h4>MALOND GRADE C</h4>
								<table class="table-produk">
									<tr>
										<td>Berat &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;165 - 184 Gram</td>
									</tr>
									<tr>
										<td>Isi/Pack &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;5 Ekor</td>
									</tr>
									<tr>
										<td>Stok &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;<?php echo $data_malond[2]->JumAkhir ?></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 pd-produk">
						<div class="produk-frame">
							<div class="img-produk">
								<img src="<?php echo base_url('asset_front/image/produk/malond_d.png'); ?>" class="img-responsive">
							</div>
							<div class="produk-name">
								<h4>MALOND GRADE D</h4>
								<table class="table-produk">
									<tr>
										<td>Berat &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;142 - 164 Gram</td>
									</tr>
									<tr>
										<td>Isi/Pack &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;10 Ekor</td>
									</tr>
									<tr>
										<td>Stok &nbsp;&nbsp;&nbsp;</td>
										<td>:</td>
										<td>&nbsp;<?php echo $data_malond[5]->JumAkhir ?> </td>
									</tr>
								</table>
							</div>
						</div>
					</div>

					

				</div>
			</div>

		</div>
	</div>

	<div class="kontak-sales" class="section bg-primary produk">
		<div class="produk-kontak">
			<h2>Dimana Mendapatkan Karkas MALOND di Sidoarjo & Mojokerto?</h2>
			<h3>Alamat Store Kami :</h3>
			<p>Wringinpitu RT 1 RW 2 bakalan wringinpitu, Balongbendo sidoarjo</p>
			<h3>CHAIRUL RACHMAD HIDAYAT</h3>
			<p>081334902657</p>
			<p>c.rhidayat13@gmail.com </p>

			<a class="btn btn-lg btn-danger" target="_target" href="tel:" role="button"> <i class="fa fa-phone"></i> Telepon</a>
			<a class="btn btn-lg btn-success" href="https://api.whatsapp.com/send?phone=6281334902657&text=Saya%20Mau%20Order" role="button"> <i class="fa fa-whatsapp"></i> Whastapp</a>
		</div>

		<div class="produk-konlocation">
			<div class="container">
				<div class="row">

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div id="googleMap" style="width:100%;height:480px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<footer id="footer" class="g-yellow">

		<!-- Container -->
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="footer-logo">
						<img src="<?php echo base_url('asset_front/image/logo_2.png'); ?>" class="img-responsive" alt="Image">
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="footer-tentang">
						<h3>Tentang Kami</h3>
						<p>
							Kami selaku distributor tunggal Malond <span class="text-wbold">Di Sidoarjo & Mojokerto Siap Menyuplai Kebutuhan Karkas Malond Untuk Usaha Kuliner Anda</span>.
							Kami juga memberi kesempatan kepada Anda untuk menjadi Agen kami di wilayah surabaya dan sekitarnya. 
						</p>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
					<div class="footer-alamat">
						<h3>Alamat Store Kami :</h3>
						<p>Wringinpitu RT 1 RW 2 bakalan wringinpitu, Balongbendo sidoarjo</p>
					</div>
					<div class="footer-kontak">
						<h3>CHAIRUL RACHMAD HIDAYAT </h3>
						<p>081334902657 <br> c.rhidayat13@gmail.com  </p>
					</div>
				</div>
				<div class="col-xs-6 col-sm-12 col-md-3 col-lg-3">
					<div class="logo-mui">
						<img src="<?php echo base_url('asset_front/image/logo_mui.png'); ?>" class="img-responsive" alt="Image">
					</div>
					<h3 class="name-mui">COPYRIGHT <br> 2018</h3>
				</div>
			</div>
		</div>
		<!-- /Container -->

	</footer>
	<!-- /Footer -->

	<div id="kontak-staytop" class="hidden-md">
		<h3>CHAIRUL RACHMAD HIDAYAT</h3>
		<p>0813-3490-2657</p>
		<div class="btn-group-horizontal">
			<a class="btn btn-danger" href="tel:081334902657" role="button"><i class="fa fa-phone"></i> Telepon</a>
			<a class="btn btn-success" target="_blank" href="https://api.whatsapp.com/send?phone=6281334902657&text=Saya%20Mau%20Order" role="button"><i class="fa fa-whatsapp"></i> Whastapp</a>
		</div>
	</div>

	<!-- Back to top -->
	<div id="back-to-top"></div>
	<!-- /Back to top -->




	<!-- jQuery Plugins -->
	<script type="text/javascript" src="<?php echo base_url('asset_front/js/jquery.min.js'); ?> "></script>
	<script type="text/javascript" src="<?php echo base_url('asset_front/js/bootstrap.min.js'); ?> "></script>
	<script type="text/javascript" src="<?php echo base_url('asset_front/js/owl.carousel.min.js'); ?> "></script>
	<script type="text/javascript" src="<?php echo base_url('asset_front/js/jquery.magnific-popup.js'); ?> "></script>
	<script type="text/javascript" src="<?php echo base_url('asset_front/js/main.js'); ?> "></script>

	<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAdWLY_Y6FL7QGW5vcO3zajUEsrKfQPNzI"></script> -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApBw7D4-p9bukMSYFQdcjSwzsYMOns5eY"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

	<script type="text/javascript">

	//Google Map
    var get_latitude  = -7.4155159;
    var get_longitude = 112.5185984;

    function initialize_google_map() {
        var myLatlng = new google.maps.LatLng(get_latitude, get_longitude);
        var mapOptions = {
            zoom: 18,
            scrollwheel: false,
            center: myLatlng
        };
        var map = new google.maps.Map(document.getElementById('googleMap'), mapOptions);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize_google_map);
	</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115465825-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-115465825-2');
</script>


</body>

</html>
