<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a href="<?php echo base_url('admin/home'); ?>"><i class="fa fa-dashboard"></i>Stock Barang</li>
    </ul>
  </div>
  <div class="menu_section">
    <h3>Setting</h3>
    <ul class="nav side-menu">
      <li><a href="<?php echo base_url('admin/out'); ?>"><i class="fa fa-sign-out"></i> Log Out</li>
    </ul>
  </div>
</div>
<!-- /sidebar menu -->