<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/inc_admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title hidden-xs">Kemitraan</a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Selamat Datang,</span>
                <h2>Nama Sales</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('admin_page/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="" alt="">Administrator
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
          <form action="" method="POST" role="form">
            <legend>Form title</legend>
          
            <div class="form-group">
              <label for="">Alamat</label>
              <textarea name="alamat" id="inputAlamt" class="form-control" rows="5" required="required"></textarea>
            </div>

            <div class="form-group">
              <label for="">Nomor Telpon 1</label>
              <input type="text" name="tlpn_1" class="form-control" id="" placeholder="Input field">
            </div>

            <div class="form-group">
              <label for="">Nomor Telpon 2</label>
              <input type="text" name="tlpn_2" class="form-control" id="" placeholder="Input field">
            </div>
          
            
            <button type="submit" class="btn btn-primary">Simpan</button>
          </form>

        </div>
        <!-- /page content -->

        <?php $this->load->view('admin_page/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/inc_admin_script.php'); ?>
  </body>
</html>
