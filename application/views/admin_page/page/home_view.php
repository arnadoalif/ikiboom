<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/inc_admin_style.php'); ?>
    <style type="text/css">
      .toggle a {
          padding: 15px 11px 0;
          margin: 0;
          cursor: pointer;
      }
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Kemitraan</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/LOGO_MALON.png'); ?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Selamat Datang,</span>
                <h2>Nama Sales</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('admin_page/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="" alt="">Administrator
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
          <!-- top tiles -->
          <div class="row tile_count">
            <?php foreach ($data_stok as $tot_malond): ?>
              
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-dashboard"></i> <?php echo str_replace('Perancis', '', $tot_malond->NamaBrg); ?></span>
              <div class="count"><?php echo number_format($tot_malond->Saldo_Pack, 0,", ", "."); ?></div>
              <span class="count_bottom"><i class="green"><?php echo number_format($tot_malond->Saldo_Ekor, 0,", ", "."); ?></i> Stok Per Ekor</span>
            </div>

            <?php endforeach ?>
            
          </div>
          <!-- /top tiles -->

          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Stok Malond</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    </p>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Nama Barang</th>
                          <th>Berat Min</th>
                          <th>Berat Max</th>
                          <th>Pack</th>
                          <th>Saldo Pack</th>
                          <th>Saldo Ekor</th>
                          <th>Saldo Pack 20%</th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php foreach ($data_stok as $dt_stok): ?>
                          
                        <tr>
                          <td><?php echo $dt_stok->NamaBrg; ?></td>
                          <td><?php echo $dt_stok->BeratMin; ?></td>
                          <td><?php echo $dt_stok->BeratMax; ?></td>
                          <td><?php echo $dt_stok->Pack; ?></td>
                          <td><?php echo number_format($dt_stok->Saldo_Pack, 0,", ", "."); ?></td>
                          <td><?php echo number_format($dt_stok->Saldo_Ekor, 0,", ", "."); ?></td>
                          <td><?php echo number_format( $dt_stok->Saldo_Ekor + ($dt_stok->Saldo_Ekor * 0.2),0,",","."); ?></td>
                        </tr>

                        <?php endforeach ?>
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>


            </div>
          </div>

        </div>
        <!-- /page content -->

        <?php $this->load->view('admin_page/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/inc_admin_script.php'); ?>
  </body>
</html>
