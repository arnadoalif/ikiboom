<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Querysharehol_model extends CI_Model {

	public function data_stok() {
		$tanggal_sekarang = date("Y-m-d");
		$tahun = date('y');
		$week = date("W", strtotime($tanggal_sekarang));
		$periode = $tahun.''.$week;
		$this->query_laba($periode);

		$sql = "
			Declare @Usr Varchar(20)='StokWeb'
			Declare @Per1 Varchar(4)='$periode'

		-- function backend sendiri load 
		-- Exec LINK_POP.mitra18.dbo.HitLabaMit2 @Usr, 'Los', @Per1, @Per1, 'All-All', 'All'

		Select E.Urut, D.KodeBrg, D.NamaBrg, D.BeratMin, D.BeratMax, D.Pack, Case When D.Pack<>0 Then Ceiling(D.Saldo_Ekor/D.Pack) Else 0 End As Saldo_Pack, D.Saldo_Ekor,@Per1 As Periode
		From(Select A.KodeBrg, C.NamaBrg, B.Nilai1 As BeratMin, B.Nilai2 As BeratMax, B.Nilai3 As Pack, SUM(A.Saldo) As Saldo_Ekor
			From(Select KodeBrg,SUM(JumAkhir) As Saldo From LINK_POP.Mitra18.dbo.AcLabaProd 
					where DbUser=@Usr and Divisi='21' and Periode=@Per1 and Jenis='Zona' and Kode_Pro='002' and Kode_Zona In (Select Kode_Zona From LINK_POP.Mitra18.dbo.RefZona Where Rpb_Prod='1' and Jenis='Zona')
					and KodeBrg In (Select KodeBrg From LINK_POP.Mitra18.dbo.Mit2Barang Where Type1='Grade' and Type2='Malond')
					Group By KodeBrg Having SUM(JumAkhir)<>0
				Union All
				Select KodeBrg,SUM(JumAkhir) As Saldo From LINK_POP.Mitra18.dbo.AcLabaProd 
					where DbUser=@Usr and Divisi='21' and Periode=@Per1 and Jenis='Zona' and Kode_Pro='005' and Kode_Zona In (Select Kode_Zona From LINK_POP.Mitra18.dbo.RefZona Where Rpb_Expe='1' and Jenis='Zona')
					and KodeBrg In (Select KodeBrg From LINK_POP.Mitra18.dbo.Mit2Barang Where Type1='Grade' and Type2='Malond')
					Group By KodeBrg Having SUM(JumAkhir)<>0
				Union All
				Select KodeBrg,SUM(JumAkhir) As Saldo From LINK_POP.Mitra18.dbo.AcLabaProd 
					where DbUser=@Usr and Divisi='21' and Periode=@Per1 and Jenis='Zona' and Kode_Pro='003' and Kode_Zona In (Select Kode_Zona From LINK_POP.Mitra18.dbo.RefZona Where Rpb_Prod='1' and Jenis='Zona')
					and KodeBrg In (Select KodeBrg From LINK_POP.Mitra18.dbo.Mit2Barang Where Type1='Grade' and Type2='Malond')
					Group By KodeBrg Having SUM(JumAkhir)<>0
				Union All
				Select KodeBrg,SUM(JumAkhir) As Saldo From LINK_POP.Mitra18.dbo.AcLabaProd 
					where DbUser=@Usr and Divisi='21' and Periode=@Per1 and Jenis='Depo' and Kode_Pro='003' and Kode_Zona In (Select Kode_Zona From LINK_POP.Mitra18.dbo.RefZona Where Rpb_Prod='1' and Jenis='Depo')
					and KodeBrg In (Select KodeBrg From LINK_POP.Mitra18.dbo.Mit2Barang Where Type1='Grade' and Type2='Malond')
					Group By KodeBrg Having SUM(JumAkhir)<>0) As A Inner Join OpenQuery(LINK_POP, 'select Jenis1, Jenis2, Nilai1, Nilai2, Nilai3 from Mitra18.dbo.Mit2Rule') As B On A.KodeBrg=B.Jenis2 Inner Join LINK_POP.Mitra18.dbo.Mit2Barang As C On A.KodeBrg=C.KodeBrg
			Where B.Jenis1='RangeBerat' and A.KodeBrg <> 'PM0004'
			Group By A.KodeBrg,C.NamaBrg,B.Nilai1,B.Nilai2,B.Nilai3) As D Inner Join LINK_POP.Mitra18.dbo.Mit2Barang As E On D.KodeBrg=E.KodeBrg
			Order By E.Urut
		 ";

		return $this->db->query($sql);
	}

	public function query_laba($periode) {
		$sql = "
				Declare @Usr Varchar(20)='StokWeb'
				Declare @Per1 Varchar(4)='$periode'
		
				-- function backend sendiri load 
				Exec LINK_POP.mitra18.dbo.HitLabaMit2 @Usr, 'Los', @Per1, @Per1, 'All-All', 'All'
		 ";
		 $this->db->query($sql);
	}


	public function data_plasma() {
		$tanggal_sekarang = date("Y-m-d");
		$tahun = date('y');
		$week = date("W", strtotime($tanggal_sekarang));
		$periode = $tahun.''.($week-1);

		$sql = "
			Declare @Per Varchar(4)='$periode'
				Select KodeBrg, SUM(Jumlah) As Jumlah from LINK_POP.Mitra18.dbo.Mit2BeliPlas Where Periode=@Per and KodeBrg In (Select KodeBrg From LINK_POP.Mitra18.dbo.Mit2Barang Where Type2='Malond')
				Group By KodeBrg
		 ";

		 return $this->db->query($sql);

	}


	public function data_produksi() {

		$tanggal_sekarang = date("Y-m-d");
		$tahun = date('y');
		$week = date("W", strtotime($tanggal_sekarang));
		$periode = $tahun.''.($week-1);

		$sql = "
			Declare @Per Varchar(4)='$periode'

				Select KodeReal,SUM(JumReal) As Jum_produksi from LINK_POP.Mitra18.dbo.Mit2Realisasi Where Periode=@Per and KodeReal In (Select KodeBrg From LINK_POP.Mitra18.dbo.Mit2Barang Where Type2='Malond')
				Group By KodeReal
		 ";
		 return $this->db->query($sql);
	}

	public function data_stok_after_produksi() {
		$tanggal_sekarang = date("Y-m-d");
		$tahun = date('y');
		$week = date("W", strtotime($tanggal_sekarang));
		$periode = $tahun.''.($week-1);

		$sql = "
			Declare @Per Varchar(4)='$periode'

				Select A.KodeProd,B.NamaBrg,SUM(A.JumProd) As Jumlah from LINK_POP.Mitra18.dbo.Mit2Realisasi As A Inner Join LINK_POP.Mitra18.dbo.Mit2Barang As B On A.KodeProd=B.KodeBrg
				Where A.Periode=@Per and B.Type2='Malond'
				Group By A.KodeProd,B.NamaBrg
		 ";
		 return $this->db->query($sql);
	}

	/**
	
		TODO:
		- Query Stok Malond Periode Saat Ini
		- Second todo item
	
	 */
	

	public function excute_laba($kode_depo) {
		$tanggal_sekarang = date("Y-m-d");
		$tahun = date('y');
		$week = date("W", strtotime($tanggal_sekarang));
		$periode = $tahun.''.($week);
		$sql_exec = "
			Declare @Per Varchar(4)='$periode'
			Declare @KdZona Varchar(3)='$kode_depo'
			Exec LINK_POP.Mitra18.dbo.HitLabaMit2 'WebMalond', 'LOS', @Per, @Per, '003-Depo', @KdZona
		 ";
		 $this->db->query($sql_exec);
	}

	public function data_saldo_stok($kode_depo) {
		$this->excute_laba($kode_depo);
		$tanggal_sekarang = date("Y-m-d");
		$tahun = date('y');
		$week = date("W", strtotime($tanggal_sekarang));
		$periode = $tahun.''.($week);
		$sql_stok = "
			Declare @Per Varchar(4)='$periode'
			Declare @KdZona Varchar(3)='$kode_depo'
			
			Select KodeBrg,NamaBrg,ISNULL((Select JumAkhir from LINK_POP.Mitra18.dbo.AcLabaProd Where DbUser='WebMalond' and Periode=@Per and KodeBrg=Z.KodeBrg),0) As JumAkhir
			From LINK_POP.Mitra18.dbo.Mit2Barang As Z Where Type1='Grade' and Type2='Malond'
		 ";
		 return $this->db->query($sql_stok);
	}

	// public function data_malond_terjual($kode_depo) {
	// 	$tanggal_sekarang = date("Y-m-d");
	// 	$tahun = date('y');
	// 	$week = date("W", strtotime($tanggal_sekarang));
	// 	$periode = $tahun.''.($week-1);
	// 	$sql ="
	// 	Declare @Per Varchar(4)='$periode'
	// 	Declare @KdZona Varchar(3)='$kode_depo'

	// 	Select KodeBrg,NamaBrg,ISNULL((Select SUM(A.JumUnit) As Jumlah from LINK_POP.Mitra18.dbo.Mit2JualCus As A  Inner Join LINK_POP.Mitra18.dbo.Mit2Product As B On A.Kode_Prod=B.Kode_Prod  Where A.Periode=@Per and A.Kode_Depo=@KdZona and B.KodeBrg=Z.KodeBrg),0) As Jumlah
	// 	From LINK_POP.Mitra18.dbo.Mit2Barang As Z Where Type1='Grade' and Type2='Malond'";

	// 	return $this->db->query($sql);
	// }

	public function data_malond_terjual($kode_depo) {
		$tanggal_sekarang = date("Y-m-d");
		$tahun = date('y');
		$week = date("W", strtotime($tanggal_sekarang));
		$periode = $tahun.''.($week-1);

		$sql = "
		Declare @Per Varchar(4)='$periode'
		Declare @KdZona Varchar(3)='$kode_depo'
		
		Select KodeBrg,NamaBrg,Hasil*5 As Hasil
		From(Select *, Case When Jumlah<>0 Then Ceiling((JumProd*(Jumlah/OmzetJatim))/5) Else 0 End As Hasil
			From(Select Z.KodeBrg,Z.NamaBrg,ISNULL((Select SUM(A.JumUnit) from LINK_POP.Mitra18.dbo.Mit2JualCus As A  Inner Join LINK_POP.Mitra18.dbo.Mit2Product As B On A.Kode_Prod=B.Kode_Prod  Where A.Periode=@Per and A.Kode_Depo=@KdZona and B.KodeBrg=Z.KodeBrg),0) As Jumlah,
					ISNULL((Select (SUM(JumProd)*60)/100 from LINK_POP.Mitra18.dbo.Mit2Realisasi Where Periode=@Per and KodeProd=Z.KodeBrg),0) As JumProd,
					ISNULL((Select SUM(A.JumUnit) from LINK_POP.Mitra18.dbo.Mit2JualCus As A  Inner Join LINK_POP.Mitra18.dbo.Mit2Product As B On A.Kode_Prod=B.Kode_Prod  Where A.Periode=@Per and A.Kode_Depo In (Select Jenis3 From LINK_POP.Mitra18.dbo.Mit2Rule Where Jenis1='DepoAktif' and Catatan1='Jatim') and B.KodeBrg=Z.KodeBrg),0) As OmzetJatim
				From LINK_POP.Mitra18.dbo.Mit2Barang As Z Where Type1='Grade' and Type2='Malond') As X) As Z

		 ";
		 return $this->db->query($sql);
	}


}

/* End of file Querysharehol_model.php */
/* Location: ./application/models/Querysharehol_model.php */