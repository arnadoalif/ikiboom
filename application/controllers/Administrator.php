<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{	
		$this->load->model('Querysharehol_model');
		$m_querysharehol = new Querysharehol_model();

		$data['data_stok'] = $m_querysharehol->data_stok()->result();
		$this->load->view('admin_page/page/home_view', $data);
	}

	public function setting() {
		$this->load->view('admin_page/page/setting_view');
	}

	public function action_setting() {
		$this->load->model('Setting_model');
		$m_setting = new Setting_model();

		$nama_member = htmlspecialchars($this->input->post('nama_member'));
		$alamat = htmlspecialchars($this->input->post('alamat'));
		$tlp_1 = htmlspecialchars($this->input->post('tlp_1'));
		$tlp_2 = htmlspecialchars($this->input->post('tlp_2'));

		$data = array(
			'nama_member' => $nama_member,
			'alamat' => $alamat,
			'tlp_1' => $tlp_1,
			'tlp_2' => $tlp_2
		);

		// $m_setting->insert_data_member('tbl_sales_member', $data);
	}

}

/* End of file Administrator.php */
/* Location: ./application/controllers/Administrator.php */