<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mojokerto extends CI_Controller {

	public function index()
	{
		/*
		310 Mojokerto
		312 Surabaya
		314 Malang
		 */
		$this->load->model('Querysharehol_model');
		$m_querysharehole = new Querysharehol_model();

		$data['data_plasma'] = $m_querysharehole->data_plasma()->row();
		$data['data_produksi'] = $m_querysharehole->data_produksi()->row();
		$data['data_stok'] = $m_querysharehole->data_stok_after_produksi()->result();
		$data['data_malond'] = $m_querysharehole->data_saldo_stok('310')->result();
		$data['data_malond_terjual'] = $m_querysharehole->data_malond_terjual('310')->result();

		
		$this->load->view('front_page/home_mojokerto', $data);
	}

}

/* End of file Mojokerto.php */
/* Location: ./application/controllers/Mojokerto.php */